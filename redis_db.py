import redis
import ast


class RedisDB(object):
    def __init__(self):
        self.db = redis.StrictRedis()

    def add_track_to_queue_list(self, spotify_id, track_metadata):
        return self.db.hset('queue_list', spotify_id, track_metadata)

    def get_track_from_queue_list(self, spotify_id):
        metadata = self.db.hget('queue_list', spotify_id)
        metadata = ast.literal_eval(metadata)
        return metadata

    def get_queue_list(self):
        return self.db.hgetall('queue_list')

    def set_last_requester(self, song, user):
        key = 'song_requester:{}'.format(song)
        return self.db.set(key, user, 3600 * 24)

    def get_last_requester(self, song):
        key = 'song_requester:{}'.format(song)
        return self.db.get(key)

    def increment_user_vetos(self, user):
        key = 'user_vetos:{}'.format(user)
        self.db.incr(key, 1)
        self.db.expire(key, 3600 * 24)

    def get_user_ban(self, user):
        key = 'user_banned:{}'.format(user)
        return True if self.db.ttl(key) > 0 else False, self.db.ttl(key)

    def increment_user_ban(self, user):
        key = 'user_banned:{}'.format(user)
        # set ban time, 5, 10, 20, 40 , 80 ... minutes
        self.db.set(key, 1, (2 ** self.get_user_vetos(user) * 150))

    def clear_user_vetos(self, user):
        key = 'user_vetos:{}'.format(user)
        self.db.delete(key)

    def get_user_vetos(self, user):
        key = 'user_vetos:{}'.format(user)
        vetos = self.db.get(key)
        if vetos:
            return int(vetos)
        return 0