from soco import SoCo
from soco.data_structures import DidlObject
from soco.exceptions import CannotCreateDIDLMetadata
from soco.xml import XML


class SoCoMuBo(SoCo):
    def add_uri_to_queue(self, uri, position=None):
        """ Adds position param to :py:meth:`~.soco.SoCo.add_uri_to_queue` """
        item = DidlObject(uri=uri, title='', parent_id='', item_id='')
        return self.add_to_queue(item, position)

    def add_to_queue(self, queueable_item, position=None):
        """ Adds position param to :py:meth:`~.soco.SoCo.add_to_queue` """
        # Check if teh required attributes are there
        for attribute in ['didl_metadata', 'uri']:
            if not hasattr(queueable_item, attribute):
                message = 'queueable_item has no attribute {0}'. \
                    format(attribute)
                raise AttributeError(message)
        # Get the metadata
        try:
            metadata = XML.tostring(queueable_item.didl_metadata)
        except CannotCreateDIDLMetadata as exception:
            message = ('The queueable item could not be enqueued, because it '
                       'raised a CannotCreateDIDLMetadata exception with the '
                       'following message:\n{0}').format(str(exception))
            raise ValueError(message)
        if isinstance(metadata, str):
            metadata = metadata.encode('utf-8')

        response = self.avTransport.AddURIToQueue([
            ('InstanceID', 0),
            ('EnqueuedURI', queueable_item.uri),
            ('EnqueuedURIMetaData', metadata),
            ('DesiredFirstTrackNumberEnqueued', position or 0),
            ('EnqueueAsNext', int(position is None))
        ])
        qnumber = response['FirstTrackNumberEnqueued']
        return int(qnumber)
