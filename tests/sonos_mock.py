class SonosMock(object):

    def __init__(self, transport_state, queue_size, playlist_position):
        self.transport_state = transport_state
        self.queue_size = queue_size
        self.playlist_position = playlist_position
        self.queue = self._generate_queue(queue_size)
        self.avTransport = AVTransportMock(self.queue)
        self.current_track_info = {'playlist_position': self.playlist_position,
                                   'uri': 'x-sonos-spotify:spotify%3Atrack%3A6OwDUCgzQOIVcjHA5PBb15?sid=12&flags=32',
                                   'title': ''}

    def _generate_queue(self, queue_size):
        queue = []
        for i in range(0, queue_size):
            queue.append(i)
        return queue

    def play(self):
        if len(self.queue) == 0:
            self.transport_state = 'STOPPED'
        else:
            self.transport_state = 'PLAYING'
        return True

    def get_current_transport_info(self):
        return {'current_transport_state': self.transport_state}

    def get_queue(self, index=0):
        queue = []
        for i in range(index, len(self.queue)):
            queue.append(self.queue[i])
        return queue

    def get_current_track_info(self):
        return self.current_track_info

    def clear_queue(self):
        self.transport_state = 'STOPPED'
        self.queue = []
        return True

    def next(self):
        self.playlist_position += 1
        self.current_track_info = {'playlist_position': self.playlist_position,
                                   'uri': 'x-sonos-spotify:spotify%3Atrack%3A21Go4aMyLGP40ANI3TU0Fn?sid=12&flags=32',
                                   'title': ''}


class AVTransportMock(object):

    def __init__(self, queue):
        self.queue = queue

    def AddURIToQueue(self, uri):
        self.queue.append(uri)
        return {'FirstTrackNumberEnqueued': len(self.queue)}
