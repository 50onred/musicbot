from __future__ import unicode_literals
import unittest
from flask import Flask
from mysql_db import MySqlDb
from poller import poll_sonos
from sonos_mock import SonosMock
from sqlalchemy import select


class TestPollerMethods(unittest.TestCase):

    def setUp(self):
        app = Flask(__name__)
        app.config.from_object('config_default')
        app.config.from_object('config_local')
        self.mysqldb = MySqlDb(app.config)
        self.day = 13
        self.night = 1

    def tearDown(self):
        self.mysqldb.conn.execute(self.mysqldb.history.delete())

    def test_poller_day_playing_not_end_of_queue(self):
        sonos = SonosMock('PLAYING', 10, 5)
        self.assertEqual(poll_sonos(self.day, sonos), False)

    def test_poller_day_playing_end_of_queue(self):
        sonos = SonosMock('PLAYING', 10, 10)
        poll_sonos(self.day, sonos)
        self.assertEqual(len(sonos.get_queue()), 11)

    def test_poller_day_not_playing_queue_empty(self):
        sonos = SonosMock('STOPPED', 0, 0)
        poll_sonos(self.day, sonos)
        self.assertEqual(len(sonos.get_queue()), 1)
        self.assertEqual(sonos.get_current_transport_info()['current_transport_state'], 'STOPPED')

    def test_poller_day_not_playing_queue_ready(self):
        sonos = SonosMock('STOPPED', 2, 1)
        poll_sonos(self.day, sonos)
        self.assertEqual(len(sonos.get_queue()), 2)
        self.assertEqual(sonos.get_current_transport_info()['current_transport_state'], 'PLAYING')

    def test_poller_night_playing_queue_not_empty(self):
        sonos = SonosMock('PLAYING', 10, 5)
        poll_sonos(self.night, sonos)
        self.assertEqual(len(sonos.get_queue()), 0)
        self.assertEqual(sonos.get_current_transport_info()['current_transport_state'], 'STOPPED')

    def test_poller_night_stopped_queue_empty(self):
        sonos = SonosMock('STOPPED', 0, 1)
        self.assertEqual(poll_sonos(self.night, sonos), False)

    def test_poller_night_stopped_queue_not_empty(self):
        sonos = SonosMock('STOPPED', 10, 10)
        self.assertEqual(poll_sonos(self.night, sonos), True)

    def test_poller_night_playing_queue_empty(self):  # in case someone is playing pandora/xm-sirius after hours
        sonos = SonosMock('PLAYING', 0, 0)
        self.assertEqual(poll_sonos(self.night, sonos), True)

    def test_day_to_night(self):
        sonos = SonosMock('PLAYING', 10, 5)
        poll_sonos(self.day, sonos)
        self.assertEqual(sonos.get_current_transport_info()['current_transport_state'], 'PLAYING')
        self.assertEqual(len(sonos.get_queue()), 10)
        poll_sonos(self.night, sonos)
        self.assertEqual(sonos.get_current_transport_info()['current_transport_state'], 'STOPPED')
        self.assertEqual(len(sonos.get_queue()), 0)

    def test_night_to_day(self):
        sonos = SonosMock('STOPPED', 0, 1)
        poll_sonos(self.night, sonos)
        self.assertEqual(sonos.get_current_transport_info()['current_transport_state'], 'STOPPED')
        self.assertEqual(len(sonos.get_queue()), 0)
        poll_sonos(self.day, sonos)
        self.assertEqual(sonos.get_current_transport_info()['current_transport_state'], 'STOPPED')
        self.assertEqual(len(sonos.get_queue()), 1)
        poll_sonos(self.day, sonos)
        self.assertEqual(sonos.get_current_transport_info()['current_transport_state'], 'STOPPED')
        self.assertEqual(len(sonos.get_queue()), 2)
        poll_sonos(self.day, sonos)
        self.assertEqual(sonos.get_current_transport_info()['current_transport_state'], 'PLAYING')
        self.assertEqual(len(sonos.get_queue()), 2)
