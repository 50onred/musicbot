from __future__ import unicode_literals
import unittest
from mysql_db import MySqlDb
from sqlalchemy import select, desc
from app import musicbot
from datetime import datetime, timedelta


class TestMySqlMethods(unittest.TestCase):

    def setUp(self):
        self.mysqldb = MySqlDb(musicbot.config)
        self.test_track_data = {
            'uri': 'spotify:track:1gv6LZiC11Z5JtyL7vdWi3',
            'title': 'True Romance',
            'artist': 'Pat Hart & the Fly Kicks',
            'explicit': False
        }
        self.test_uri_1 = 'spotify:track:1img0Dh78EKEMyngCKKgTA'
        ins = self.mysqldb.playlist.insert().values(uri=self.test_uri_1,
                                                    title='South Street',
                                                    artist='One Nine Crew',
                                                    last_added_by='bob',
                                                    popularity=3,
                                                    explicit=False)
        self.mysqldb.conn.execute(ins)
        self.test_uri_2 = 'spotify:track:2VkWpcEJ1I016fYVXbX0dI'
        ins = self.mysqldb.playlist.insert().values(uri=self.test_uri_2,
                                                    title='Be Alright',
                                                    artist='One Nine Crew',
                                                    last_added_by='bob',
                                                    popularity=0,
                                                    explicit=False)
        self.mysqldb.conn.execute(ins)
        self.test_uri_3 = 'spotify:track:23Wa1CTGEOVEoSG26XiFec'
        ins = self.mysqldb.playlist.insert().values(uri=self.test_uri_3,
                                                    title='No Delay',
                                                    artist='One Nine Crew',
                                                    last_added_by='bob',
                                                    popularity=5,
                                                    explicit=False)
        self.mysqldb.conn.execute(ins)
        results = self.mysqldb.conn.execute(select([self.mysqldb.playlist])
                                            .order_by('last_added_at'))
        row = results.first()
        upd = self.mysqldb.playlist.update().where(self.mysqldb.playlist.c.id == row['id'])\
            .values(explicit=True)
        self.mysqldb.conn.execute(upd)

        # adds some history for testing user add count
        current_time = datetime.utcnow()
        for i in range(0, 30, 5):
            timestamp = current_time + timedelta(minutes=i)
            ins = self.mysqldb.history.insert().values(action='add',
                                               user='bob',
                                               timestamp=timestamp)
            self.mysqldb.conn.execute(ins)

    def tearDown(self):
        self.mysqldb.conn.execute(self.mysqldb.playlist.delete()
                                  .where(self.mysqldb.playlist.c.uri == self.test_track_data['uri']))
        self.mysqldb.conn.execute(self.mysqldb.playlist.delete()
                                  .where(self.mysqldb.playlist.c.uri == self.test_uri_1))
        self.mysqldb.conn.execute(self.mysqldb.playlist.delete()
                                  .where(self.mysqldb.playlist.c.uri == self.test_uri_2))
        self.mysqldb.conn.execute(self.mysqldb.playlist.delete()
                                  .where(self.mysqldb.playlist.c.uri == self.test_uri_3))
        self.mysqldb.conn.execute(self.mysqldb.history.delete())

    def test_get_early_tracks(self):
        tracks = self.mysqldb.get_early_tracks(5, True)
        self.assertTrue(type(tracks) is list)
        self.assertIs(len(tracks), 5)
        self.assertTrue(type(tracks[0]) is dict)
        self.assertTrue(len(tracks[0]['uri']) == 36)
        self.assertTrue(type(tracks[0]['popularity'] is int))
        self.assertFalse(tracks[0]['explicit'])

    def test_add_new_track(self):
        self.assertEqual(self.mysqldb.add_new_track(self.test_track_data, 'bob'), 'inserted')
        results = self.mysqldb.conn.execute(select([self.mysqldb.playlist])
                                            .where(self.mysqldb.playlist.c.uri == self.test_track_data['uri']))
        row = results.first()
        self.assertEqual(row['uri'], self.test_track_data['uri'])
        self.assertEqual(self.mysqldb.add_new_track(self.test_track_data, 'nick'), 'updated')
        results = self.mysqldb.conn.execute(select([self.mysqldb.playlist])
                                            .where(self.mysqldb.playlist.c.uri == self.test_track_data['uri']))
        row = results.first()
        self.assertEqual(row['last_added_by'], 'nick')

    def test_get_track_by_uri(self):
        ins = self.mysqldb.playlist.insert().values(uri=self.test_track_data['uri'],
                                                    title=self.test_track_data['title'],
                                                    artist=self.test_track_data['artist'])
        self.mysqldb.conn.execute(ins)
        track = self.mysqldb.get_track_by_uri(self.test_track_data['uri'])
        self.assertEqual(track['title'], self.test_track_data['title'])

    def test_decrease_popularity(self):
        self.assertEqual(self.mysqldb.decrease_popularity(self.test_uri_1), 2)
        self.assertEqual(self.mysqldb.decrease_popularity(self.test_uri_2), 0)
        results = self.mysqldb.conn.execute(select([self.mysqldb.playlist])
                                            .where(self.mysqldb.playlist.c.uri == self.test_uri_1))
        row = results.first()
        self.assertEqual(row['popularity'], 2)
        results = self.mysqldb.conn.execute(select([self.mysqldb.playlist])
                                            .where(self.mysqldb.playlist.c.uri == self.test_uri_2))
        row = results.first()
        self.assertEqual(row['popularity'], 0)

    def test_increase_popularity(self):
        self.assertEqual(self.mysqldb.increase_popularity(self.test_uri_1), 4)
        self.assertEqual(self.mysqldb.increase_popularity(self.test_uri_3), False)
        results = self.mysqldb.conn.execute(select([self.mysqldb.playlist])
                                            .where(self.mysqldb.playlist.c.uri == self.test_uri_1))
        row = results.first()
        self.assertEqual(row['popularity'], 4)
        results = self.mysqldb.conn.execute(select([self.mysqldb.playlist])
                                            .where(self.mysqldb.playlist.c.uri == self.test_uri_3))
        row = results.first()
        self.assertEqual(row['popularity'], 5)

    def test_update_time_and_user(self):
        self.mysqldb.update_time_and_user(self.test_uri_1, 'MusicBot')

    def test_record_event(self):
        self.mysqldb.record_event('veto', user='bob', uri=self.test_uri_1)
        results = self.mysqldb.conn.execute(select([self.mysqldb.history]).order_by(desc(self.mysqldb.history.c.id)))
        row = results.first()
        self.assertEqual(row['action'], 'veto')
        self.assertEqual(row['user'], 'bob')
        self.assertEqual(row['uri'], self.test_uri_1)

    def test_delete_track_by_uri(self):
        self.mysqldb.delete_track_by_uri(self.test_uri_1);
        results = self.mysqldb.conn.execute(select([self.mysqldb.playlist])
                                            .where(self.mysqldb.playlist.c.uri == self.test_uri_1))
        self.assertTrue(results.rowcount == 0)
