# coding=utf8

from __future__ import unicode_literals
import unittest
from command_handler import CommandHandler
from flask import Flask
from musicbot import MusicBot
from sonos_mock import SonosMock

class TestCommandHandler(unittest.TestCase):

    def setUp(self):
        app = Flask(__name__)
        app.config.from_object('config_default')
        app.config.from_object('config_local')
        self.config = app.config
        self.sonos = SonosMock('PLAYING', 10, 5)
        self.musicbot = MusicBot(app.config, self.sonos)
        self.ch = CommandHandler(app.config, self.musicbot, self.sonos)

    def test_command_handler_current(self):
        returned_string = u'> Now playing: MØ - Say You\'ll Be There (selected by MusicBot) :star::star::star:'
        self.assertEqual(self.ch.command_handler('current', 'bob'), returned_string)

    def test_explicit_text(self):
        self.assertEqual(self.ch.explicit_text(True), self.config['EXPLICIT_TEXT'])
        self.assertEqual(self.ch.explicit_text(False), '')

    def test_convert_hour(self):
        self.assertEqual(self.ch.convert_hour(0), '12 am')
        self.assertEqual(self.ch.convert_hour(2), '2 am')
        self.assertEqual(self.ch.convert_hour(12), '12 pm')
        self.assertEqual(self.ch.convert_hour(14), '2 pm')
        with self.assertRaises(ValueError):
            self.ch.convert_hour(24)
            self.ch.convert_hour('string')

    def test_sanitize_command(self):
        with_uppercase = 'Upnext'
        with_trailing_spaces = ' upnext '
        lowercase = 'upnext'
        self.assertEqual(self.ch.sanitize_command(with_uppercase), lowercase)
        self.assertEqual(self.ch.sanitize_command(with_trailing_spaces), lowercase)
        self.assertEqual(self.ch.sanitize_command(lowercase), lowercase)
