from __future__ import unicode_literals
import random, unittest
from copy import deepcopy
from flask import Flask
from musicbot import MusicBot
from mysql_db import MySqlDb
from sonos_mock import SonosMock
from sqlalchemy import select


class TestMusicbotMethods(unittest.TestCase):

    def setUp(self):
        app = Flask(__name__)
        app.config.from_object('config_default')
        app.config.from_object('config_local')
        self.config = app.config
        self.sonos = SonosMock('PLAYING', 10, 5)
        self.musicbot = MusicBot(app.config, self.sonos)
        self.mysqldb = MySqlDb(app.config)
        self.db_track_data = {
            'uri': 'spotify:track:1gv6LZiC11Z5JtyL7vdWi3',
            'title': 'True Romance',
            'artist': 'Pat Hart & the Fly Kicks',
            'explicit': False
        }
        self.sonos_track_data_musicbot = {
            'uri': 'x-sonos-spotify:spotify%3Atrack%3A6OwDUCgzQOIVcjHA5PBb15?sid=12&flags=32',
            'title': ''
        }
        self.sonos_track_data_other = {
            'uri': '',
            'title': 'A Pandora Track'
        }
        self.mysqldb.add_new_track(self.db_track_data, 'bob')

    def tearDown(self):
        self.mysqldb.conn.execute(self.mysqldb.playlist.delete()
                                  .where(self.mysqldb.playlist.c.uri == self.db_track_data['uri']))
        self.mysqldb.conn.execute(self.mysqldb.playlist.delete()
                                  .where(self.mysqldb.playlist.c.uri == 'spotify:track:5AEaBZSP8YDkYlJWCyWPez'))
        self.mysqldb.conn.execute(self.mysqldb.playlist.delete()
                                  .where(self.mysqldb.playlist.c.uri == 'spotify:track:7Ie9W94M7OjPoZVV216Xus'))
        self.mysqldb.conn.execute(self.mysqldb.playlist.update()
                                  .where(self.mysqldb.playlist.c.uri == 'spotify:track:6OwDUCgzQOIVcjHA5PBb15')
                                  .values(popularity=3))
        self.mysqldb.conn.execute(self.mysqldb.history.delete())

    def search_history_by(self, key, value):
        if key == 'uri':
            column = self.mysqldb.history.c.uri
        elif key == 'user':
            column = self.mysqldb.history.c.user
        elif key == 'action':
            column = self.mysqldb.history.c.action
        else:
            return False
        results = self.mysqldb.conn.execute(select([self.mysqldb.history])
                                            .where(column == value))
        return results

    def test_add_random_track(self):
        random_track = self.musicbot.add_random_track(self.sonos)
        self.assertEqual(len(random_track), 36)
        self.assertEqual(len(self.sonos.get_queue()), 11)
        history = self.search_history_by('uri', random_track).first()
        self.assertEqual(history['action'], 'add')
        self.assertEqual(history['user'], 'MusicBot')
        self.assertEqual(history['uri'], random_track)

    def test_add_uri_to_sonos(self):
        queue_size = len(self.sonos.get_queue())
        self.assertEqual(self.musicbot.add_uri_to_sonos('spotify:track:2iG6SMdF1c0PgSi9mtgmtu', self.sonos),
                         queue_size + 1)

    def test_add_chosen_track(self):
        add_request = [{
            'user': 'bob',
            'results': [{
                'title': '31 Days',
                'artist': 'One Nine Crew',
                'uri': 'spotify:track:5AEaBZSP8YDkYlJWCyWPez',
                'explicit': False
            }]
        }]
        saved_request = deepcopy(add_request)
        self.musicbot.add_requests = add_request
        queue_size = len(self.sonos.get_queue())
        self.musicbot.add_chosen_track('1', 'bob', self.sonos, 12)
        self.assertEqual(len(self.sonos.get_queue()), queue_size + 1)
        history = self.search_history_by('uri', saved_request[0]['results'][0]['uri']).first()
        self.assertEqual(history['action'], 'add')
        self.assertEqual(history['user'], saved_request[0]['user'])
        self.assertEqual(history['uri'], saved_request[0]['results'][0]['uri'])

    def test_add_chosen_explicit_track(self):
        add_request = [{
            'user': 'bob',
            'results': [{
                'title': 'Not Afraid',
                'artist': 'Eminem',
                'uri': 'spotify:track:7Ie9W94M7OjPoZVV216Xus',
                'explicit': True
            }]
        }]
        random_work_hour = random.randrange(self.config['WORK_HOURS_START'], self.config['WORK_HOURS_END'])
        random_nonwork_hour = random.randrange(self.config['WORK_HOURS_END'], 24)
        self.musicbot.add_requests = add_request
        queue_size = len(self.sonos.get_queue())
        self.musicbot.add_chosen_track('1', 'bob', self.sonos, random_work_hour)
        self.assertEqual(len(self.sonos.get_queue()), queue_size)
        self.musicbot.add_chosen_track('1', 'bob', self.sonos, random_nonwork_hour)
        self.assertEqual(len(self.sonos.get_queue()), queue_size + 1)

    def test_get_current_track(self):
        track_info = self.musicbot.get_current_track(self.sonos)
        self.assertIsInstance(track_info, dict)

    def test_get_spotify_track(self):
        response = self.musicbot.get_spotify_track(self.db_track_data['uri'])
        self.assertEqual(response['name'], self.db_track_data['title'])

    def test_is_work_hours(self):
        random_work_hour = random.randrange(self.config['WORK_HOURS_START'], self.config['WORK_HOURS_END'])
        random_nonwork_hour_am = random.randrange(0, self.config['WORK_HOURS_START'])
        random_nonwork_hour_pm = random.randrange(self.config['WORK_HOURS_END'], 24)
        self.assertTrue(self.musicbot.is_work_hours(random_work_hour))
        self.assertTrue(self.musicbot.is_work_hours(self.config['WORK_HOURS_START']))
        self.assertFalse(self.musicbot.is_work_hours(random_nonwork_hour_am))
        self.assertFalse(self.musicbot.is_work_hours(random_nonwork_hour_pm))
        self.assertFalse(self.musicbot.is_work_hours(self.config['WORK_HOURS_END']))

    def test_get_track_metadata(self):
        self.assertEqual(self.musicbot.get_track_metadata(self.sonos_track_data_musicbot)['title'],
                         'Say You\'ll Be There')
        self.assertEqual(self.musicbot.get_track_metadata(self.sonos_track_data_musicbot)['popularity'], 3)
        self.assertEqual(self.musicbot.get_track_metadata(self.sonos_track_data_other)['title'], 'A Pandora Track')
        self.assertEqual(self.musicbot.get_track_metadata(self.sonos_track_data_other)['popularity'], 0)

    def test_vetoes(self):
        current_track = self.musicbot.get_track_metadata(self.sonos.get_current_track_info())
        # first veto by bob
        self.assertEqual(self.musicbot.veto('bob'),
                         (1, current_track))
        # second veto by bob - doesn't count
        self.assertEqual(self.musicbot.veto('bob'),
                         (False, self.musicbot.get_track_metadata(self.sonos.get_current_track_info())))
        # second veto by max
        self.assertEqual(self.musicbot.veto('max'),
                         (2, current_track))
        # third veto by irem
        current_track['popularity'] -= 1
        self.assertEqual(self.musicbot.veto('irem'),
                         (3, current_track))
        self.assertEqual(self.sonos.playlist_position, 6)
        self.assertEqual(self.search_history_by('action', 'veto').rowcount, 3)
        self.assertEqual(self.search_history_by('action', 'popularity_decreased').rowcount, 1)
        # first veto on new track by dave
        current_track = self.musicbot.get_track_metadata(self.sonos.get_current_track_info())
        self.assertEqual(self.musicbot.veto('dave'),
                         (1, current_track))

    def test_upvotes(self):
        current_track = self.musicbot.get_track_metadata(self.sonos.get_current_track_info())
        # first upvote by bob
        self.assertEqual(self.musicbot.upvote('bob'),
                         (1, current_track))
        # second upvote by bob - doesn't count
        self.assertEqual(self.musicbot.upvote('bob'),
                         (False, self.musicbot.get_track_metadata(self.sonos.get_current_track_info())))
        # second upvote by max
        self.assertEqual(self.musicbot.upvote('max'),
                         (2, current_track))
        # third upvote by irem
        current_track['popularity'] += 1
        self.assertEqual(self.musicbot.upvote('irem'),
                         (3, current_track))
        self.assertEqual(self.search_history_by('action', 'upvote').rowcount, 3)
        self.assertEqual(self.search_history_by('action', 'popularity_increased').rowcount, 1)
        # first upvote on new track by dave
        self.sonos.next()
        current_track = self.musicbot.get_track_metadata(self.sonos.get_current_track_info())
        self.assertEqual(self.musicbot.upvote('dave'),
                         (1, current_track))

    def test_validate_uri(self):
        invalid_uri = "7liNRTr5Px7I3R96mDucjw"
        valid_uri = "1kI11NjmGwJgRjDfOLoDwO"
        bad_uri = "1KI11NjmGwJgRjDfOLoDwO"
        self.assertFalse(self.musicbot.is_valid_uri(invalid_uri))
        self.assertTrue(self.musicbot.is_valid_uri(valid_uri))
        self.assertFalse(self.musicbot.is_valid_uri(bad_uri))
