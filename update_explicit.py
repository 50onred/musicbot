from flask import Flask
from sqlalchemy import *
from musicbot import MusicBot
from time import sleep

app = Flask(__name__)

app.config.from_object('config_default')
app.config.from_object('config_local')

musicbot = MusicBot(app.config, {})

engine = create_engine(app.config['SQLALCHEMY_DATABASE_URI'], encoding='utf8')
meta = MetaData()
playlist = Table('playlist', meta, autoload=True, autoload_with=engine)
conn = engine.connect()

tracks_to_update = []

s = select([playlist.c.uri])
results = conn.execute(s)

for row in results:
    response = musicbot.get_spotify_track(row['uri'])
    try:
        print response['explicit']
    except KeyError:
        print 'waiting for api to cool down'
        sleep(10)
        response = musicbot.get_spotify_track(row['uri'])
    if response['explicit']:
        print 'appending to bad list'
        tracks_to_update.append(row['uri'])

results.close()

for track in tracks_to_update:
    upd = playlist.update().where(playlist.c.uri == track).values(explicit=True)
    conn.execute(upd)
