from __future__ import unicode_literals

# Default local settings
# NOTE: Create config_local.py in the project director, and override the settings below there.
DEVICE_IP = '192.168.10.191'  # the IP address of the sonos device
PLAYLIST_USER_ID = 'spotify'  # the name of the owner of the initial seed playlist
PLAYLIST_ID = '5FJXhjdILmRA2z5bvz4nzf'  # the uri of the initial seed playlist
# Visit https://developer.spotify.com/web-api/authorization-guide/ for info on these next two settings
SPOTIFY_CLIENT_ID = ''
SPOTIFY_CLIENT_SECRET = ''
ADMINS = []  # list of users who will have access to admin commands
SQLALCHEMY_DATABASE_URI = 'mysql://root@localhost/musicbot'  # MySQL credentials and database name
HOST = '127.0.0.1'
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Settings

VETOES_REQUIRED = 3  # number of unique users required to veto a track
UPVOTES_REQUIRED = 3  # number of unique users required to upvote a track
QUEUE_LENGTH = 10  # number of upcoming tracks to display for `list` command
COUNTRY_CODE = "US"  # country code filter (sonos will only play tracks in your locale)
POLLING_TIME = 10  # interval in seconds to poll the sonos to device to see if a new track needs to be added

# hours between which the poller will run and music will play. At NIGHT_HOUR, the poller stop playback, clear down
# the queue, and stop polling the Sonos. At DAY_HOUR, the poller will add tracks to the queue, start playback, and
# resume queuing the Sonos.
NIGHT_HOUR = 20
DAY_HOUR = 8

# Work hours - explicit tracks will be blocked from being added to the playlist during these hours
WORK_HOURS_START = 9
WORK_HOURS_END = 17

# Add restrictions - prevents users from adding too many tracks to the queue at one
ADD_RESTRICTION_TIMEFRAME = 15  # in minutes, the window of time in which users previous adds will be counted
ADD_RESTRICTION_LIMIT = 3  # users with this many adds in the timeframe will be blocking from adding more tracks

# when selecting a random track, the number of earliest tracks that musicbot will randomly select# from. Higher numbers
# will result in more frequent repeat tracks, lower numbers result in a less random experience. The proper setting will
# depend on how many track are in your playlist table.
EARLY_LIMIT = 100

# Messages
# These are the responses musicbot will output to slack for various commands.

MSG_PREPEND = "> "  # Characters to prepend to every message

SPOTIFY_TRACK_FORMAT = "{0} - {1} {2}"  # For responses from spotify {0} = Artist, {1} = Title, {2} = Explicit
SONOS_TRACK_FORMAT = "{0} - {1} (selected by {2}) {3}"  # For responses from {0} = Artist, {1} = Title, {2} = User,
                                                        # {3} = Stars

MUSIC_PLAY = "Music is playing!"
MUSIC_PAUSE = "Music is paused."
VOLUME_INCREASE = "Volume increased to {0}"  # Volume level 1-100
VOLUME_DOWN = "Volume decreased to {0}"  # Volume level 1-100
VOLUME_MAX = "Volume already at max level."
VOLUME_MIN = "Volume already at min level."
NEXT_TRACK = "Skipped to next track"
PREV_TRACK = "Went back to previous track"
NO_SONG_PLAYING = "No song currently playing"
TRACK_PLAYING = "Now playing: {0}"  # Sonos track format
CLEAR_QUEUE_SUCCESS = "Queue was successfully cleared"
CLEAR_QUEUE_FAILURE = "Queue was not cleared, sorry!"
CURRENT_TRACK_REMOVED = "Current track removed from queue"
TRACK_REMOVED = "Track removed from queue"
TRACK_VETOED = "Track \"{0}\" vetoed {1} time(s)."  # {0} = Title, {1} = Veto Count
TRACK_UPVOTED = "Track \"{0}\" upvoted {1} time(s)."  # {0} = Title, {1} = Upvote Count
TRACK_POP_INCREASE = " \"{0}\"'s popularity has increased to {1}."  # {0} = Title, {1} = star count
NO_MORE_UPVOTES = " Sorry, but there's too much love for \"{0}\". It already has 5 stars."  # {0} = Title
UPVOTING_CLOSED = " Sorry, but upvoting is closed until the next track."
TRACK_VETO_REMOVE = " Skipping to next track! \"{0}\"'s popularity has decreased to {1}. " \
                    "http://www.reactiongifs.com/wp-content/uploads/2011/12/Gladiator_Thumb_Down_01.gif"  # {0} = Title
USER_ALREADY_VETOED = "Sorry {0}, you have already vetoed the track \"{1}\"."  # {0} = User, {1} = Title
USER_ALREADY_UPVOTED = "Sorry {0}, you have already upvoted the track \"{1}\"."  # {0} = User, {1} = Title
SEARCH_SELECT_INSTRUCTION = "Enter the number of your selection out of the search results. Example: `1`"
TRACK_CHOSEN = "{0} added to the queue"  # {0} Spotify track format
NO_RESULTS = "Sorry, I can't find that track. Please try another search."
EXPLICIT_TEXT = '[EXPLICIT]'
EXPLICIT_WARNING = 'Sorry {0}, but explicit tracks cannot be added between the hours of {1} and {2}'
# {0} = User {1} = day hour {2} = night hour
PURGE_COMPLETE = "Queue has been cleared. Playback will automatically resume in approximately 20 seconds."

# Commands
# Lists to customize commands that count as upvotes and downvotes. Slack emojis are surrounded by : :"
UPVOTE_COMMANDS = [':+1:', ':thumbsup:', 'upvote', ':star:', ':thumbsup::skin-tone-2:', ':thumbsup::skin-tone-3:',
                   ':thumbsup::skin-tone-4:', ':thumbsup::skin-tone-5:', ':thumbsup::skin-tone-6:', ':arrow_up:',
                   ':upvote:', ':up', ':arrow_up_small:', ':arrow_double_up:', ':arrow_heading_up:', ':boom:',
                   ':point_up:', ':point_up_2:', ':small_red_triangle_up:', ':small_red_triangle:',
                   ':+1::skin-tone-2:', ':+1::skin-tone-3:', ':+1::skin-tone-4:', ':+1::skin-tone-5:',
                   ':+1::skin-tone-6:', '+1', ':metal:', ':collision:']
VETO_COMMANDS = [':-1:', ':thumbsdown:', 'veto', 'downvote', ':thumbsdown::skin-tone-2:', ':thumbsdown::skin-tone-3:',
                 ':thumbsdown::skin-tone-4:', ':thumbsdown::skin-tone-5:', ':thumbsdown::skin-tone-6:', ':arrow_down:',
                 ':downvote:', ':arrow_down_small:', ':arrow_double_down:', ':arrow_heading_down:', '-1',
                 ':-1::skin-tone-2:', ':-1::skin-tone-3:', ':-1::skin-tone-4:', ':-1::skin-tone-5:',
                 ':-1::skin-tone-6:', ':poop:', ':angrypoop:']



# help info
HELP = """> `current` gets the currently playing track.
> `add [search query]` to search Spotify for track to add.
> `[number]` to select song from returned search results.
> `list` to display next {} songs in queue
> `vol+` to increase volume.
> `vol-` to decrease volume.
> `help` or `man` to see this man page.""".format(QUEUE_LENGTH)
ADMIN_HELP = """\n> ADMIN ONLY COMMANDS:
> `pause` to pause playback.
> `play` to resume playback.
> `next` to skip to next track.
> `previous` to skip to previous track.
> `remove` to remove currently-playing track.
> `remove [index]` to remove track index from current position. `0` is the current track, `1` is next, etc."""
