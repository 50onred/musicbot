# Musicbot

A slackbot for controlling the music in the office.

## Local development instructions
 - Make sure mysql and redis are installed and running
 - Create a mysql database called musicbot
 - Clone the repo, create a virtual environment, and run `pip install -r requirements.txt`
 - Create a `config_local.py` file, and configure for your local environment. See `config_default.py` for further 
   instructions setting up your local config.
 - Once you have configured everything, run the following commands to provision the database:
   - `python app.py db upgrade` will run all migrations and set up the playlist table
   - `python app.py seed_db` will seed the database with tracks from the spotify playlist referenced in `config_local.py`
 - Run `python app.py runserver` to start the server.
 - When running locally, you will not actually be able to interact with Slack, so there is a test route set up for trying
   out commands. Send POST requests to `localhost:2000/test/<command>?user=<user>`, to interact with musicbot.
 - *About the Poller:* There is a separate python script `poller.py` that when running will poll the sonos device at 
   regular intervals to determine if new tracks need to be added to the playlist. Running this script is not necessary 
   for local development, but when running will call `MusicBot.add_random_track()` if there are no tracks left to play.

## Testing
 - Tests are in the `tests/` folder and are broken into suites according to the classes that they test. Tests use the
   `SonosMock` class to emulate the Sonos API, so tests will not interfere with the current Sonos playback. 
   Tests that write to the database will cleanup after themselves as well.
