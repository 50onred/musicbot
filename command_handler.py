from __future__ import unicode_literals
from datetime import datetime
import re


class CommandHandler(object):

    def __init__(self, config, musicbot):
        self.config = config
        self.musicbot = musicbot

    def is_admin(self, user):
        return user in self.config['ADMINS']

    def assemble_msg(self, msg):
        return self.config["MSG_PREPEND"] + msg

    def format_sonos_track(self, track):
        stars = self.get_stars_as_emoji(track['popularity'])
        return self.config['SONOS_TRACK_FORMAT'].format(track.get('artist', 'Unknown').decode('utf-8'),
                                                        track.get('title', 'Unknown').decode('utf-8'),
                                                        track.get('last_added_by', 'Unknown'),
                                                        stars)

    def get_stars_as_emoji(self, popularity):
        stars = ''
        for i in range(0, popularity):
            stars += ':star:'
        return stars

    def get_stars_as_number(self, popularity):
        if popularity == 1:
            return str(popularity) + ' star'
        return str(popularity) + ' stars'

    def explicit_text(self, explicit):
        """
        returns the configured explicit tag if true, an empty string if false
        """
        if explicit:
            return self.config['EXPLICIT_TEXT']
        return ''

    def convert_hour(self, hour):
        """
        converts an integer between 0 and 23 to an 12-hour format string
        """
        if hour == 0:
            return '12 am'
        if 1 <= hour < 12:
            return '{0} am'.format(str(hour))
        if hour == 12:
            return '12 pm'
        if 13 <= hour <= 23:
            return '{0} pm'.format(str(hour - 12))
        raise ValueError('Hour must be an integer between 0 and 23')

    def sanitize_command(self, command):
        return command.lower().strip()

    def command_handler(self, command, user):

        command = self.sanitize_command(command)

        if command == 'purge' and self.is_admin(user):
            self.musicbot.clear_queue()
            return self.assemble_msg(self.config["PURGE_COMPLETE"])
        if command == 'play' and self.is_admin(user):
            self.musicbot.play()
            return self.assemble_msg(self.config["MUSIC_PLAY"])
        if command == 'pause' and self.is_admin(user):
            self.musicbot.pause()
            return self.assemble_msg(self.config["MUSIC_PAUSE"])
        if command == 'vol+':
            new_vol = self.musicbot.vol_up()
            if new_vol:
                return self.assemble_msg(self.config["VOLUME_INCREASE"].format(str(new_vol)))
            return self.assemble_msg(self.config["VOLUME_MAX"])
        if command == 'vol-':
            new_vol = self.musicbot.vol_down()
            if new_vol:
                return self.assemble_msg(self.config["VOLUME_DOWN"].format(str(new_vol)))
            return self.assemble_msg(self.config["VOLUME_MIN"])
        if command == 'next' and self.is_admin(user):
            self.musicbot.next_track()
            return self.assemble_msg(self.config["NEXT_TRACK"])

        if command == 'previous' and self.is_admin(user):
            self.musicbot.prev_track()

            return self.assemble_msg(self.config["PREV_TRACK"])
        if command == 'remove' and self.is_admin(user):
            self.musicbot.remove_current_from_queue()

            return self.assemble_msg(self.config["CURRENT_TRACK_REMOVED"])
        if re.match('remove\s\d*', command) and self.is_admin(user):
            index = re.sub('remove', '', command).strip()
            self.musicbot.remove_index_from_queue(index)

            return self.assemble_msg(self.config["TRACK_REMOVED"])
        if command == 'current':
            track = self.musicbot.get_current_track()
            formatted_track = self.format_sonos_track(track)
            return self.assemble_msg(self.config["TRACK_PLAYING"].format(formatted_track))
        if command.startswith('add'):
            query = re.sub('add', '', command).strip()
            songs = self.musicbot.search_for_track(query, user)

            to_return = ""
            curr_num = 1

            banned, banned_string = self.musicbot.is_user_banned(user)
            if banned:
                return "Sorry, you are banned from adding songs for {}".format(banned_string)

            if not songs:
                return self.config['NO_RESULTS']

            for song in songs:
                formatted = self.config["SPOTIFY_TRACK_FORMAT"].format(song['artist'],
                                                                       song['title'],
                                                                       self.explicit_text(song['explicit']))
                to_return += self.assemble_msg("{0}. {1}\n".format(str(curr_num), formatted))
                curr_num += 1

            to_return += self.assemble_msg(self.config["SEARCH_SELECT_INSTRUCTION"])
            return to_return
        if command == 'list':
            tracks = self.musicbot.get_next_on_queue()
            track_list = ''
            formatted_tracks = []
            for track in tracks:
                formatted_track = self.format_sonos_track(track)
                formatted_tracks.append(formatted_track)
            for track in enumerate(formatted_tracks, start=1):
                track_list += '> {0}. {1}\n'.format(track[0], track[1])
            return track_list
        if re.match('[12345]', command):
            song = self.musicbot.add_chosen_track(command, user, datetime.now().time().hour)
            if song == 'explicit_track':
                return self.config['EXPLICIT_WARNING'].format(user,
                                                              self.convert_hour(self.config['WORK_HOURS_START']),
                                                              self.convert_hour(self.config['WORK_HOURS_END']))
            if song:
                formatted = self.config["SPOTIFY_TRACK_FORMAT"].format(song['artist'],
                                                                       song['title'],
                                                                       self.explicit_text(song['explicit']))
                return self.assemble_msg(self.config["TRACK_CHOSEN"].format(formatted))
        if command == 'man':
            return self.musicbot.man(self.is_admin(user))
        if command in self.config['VETO_COMMANDS']:
            response, track_data = self.musicbot.veto(user)
            if response:
                to_return = self.config["TRACK_VETOED"].format(track_data['title'], response)

                if response == self.config["VETOES_REQUIRED"]:
                    stars = self.get_stars_as_number(track_data['popularity'])
                    to_return += self.config["TRACK_VETO_REMOVE"].format(track_data['title'], stars)

                return self.assemble_msg(to_return)
            return self.assemble_msg(self.config["USER_ALREADY_VETOED"].format(user, track_data['title']))

        if command in self.config['UPVOTE_COMMANDS']:
            response, track_data = self.musicbot.upvote(user)
            if response:
                to_return = self.config["TRACK_UPVOTED"].format(track_data['title'], response)

                if response == self.config["UPVOTES_REQUIRED"]:
                    if not track_data['popularity']:
                        to_return += self.config['NO_MORE_UPVOTES']
                    else:
                        stars = self.get_stars_as_number(track_data['popularity'])
                        to_return += self.config["TRACK_POP_INCREASE"].format(track_data['title'], stars)
                if response > self.config['UPVOTES_REQUIRED']:
                    to_return = self.config['UPVOTING_CLOSED']
                return self.assemble_msg(to_return)
            return self.assemble_msg(self.config["USER_ALREADY_UPVOTED"].format(user, track_data['title']))

        if command == 'help':
            return self.musicbot.man(self.is_admin(user))

        return ''
