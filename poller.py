from __future__ import unicode_literals
import time
from musicbot import MusicBot
from flask import Flask
from datetime import datetime
from raven.contrib.flask import Sentry
from socomubo import SoCoMuBo
from soco import SoCoException

app = Flask(__name__)
app.config.from_object('config_default')
app.config.from_object('config_local')
sonos = SoCoMuBo(app.config['DEVICE_IP'])
sentry = Sentry(app)

musicbot = MusicBot(app.config, sonos)

def poll_sonos(current_hour):
    if app.config['DAY_HOUR'] <= current_hour < app.config['NIGHT_HOUR']:
        tracks_remaining = musicbot.get_queue_length()
        if musicbot.get_playback_state() == 'STOPPED' and tracks_remaining > 0:
            try:
                musicbot.play()
            except SoCoException:
                return False
        if tracks_remaining == 0:
            return musicbot.add_random_track()
        else:
            return False


def main():
    while True:
        current_hour = datetime.now().time().hour
        resp = poll_sonos(current_hour)
        time.sleep(app.config['POLLING_TIME'])


if __name__ == '__main__':
    main()
