from urllib import quote_plus


class QTrack(object):

    """ Class that represents a Spotify track
    usage example: SpotifyTrack('spotify:track:20DfkHC5grnKNJCzZQB6KC') """

    def __init__(self, spotify_uri):
        self.URI = spotify_uri
        self.title = ""
        self.albumURI = ""

    def didl_metadata(self, metadata_format):
        """ DIDL Metadata """
        didl_metadata = metadata_format.format(self.URI, self.albumURI, self.title)
        didl_metadata = didl_metadata.encode('utf-8')
        return didl_metadata

    @property
    def uri(self):
        return 'x-sonos-spotify:' + quote_plus(self.URI) + '?sid=12&flags=32'  # &sn=8
