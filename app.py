from __future__ import unicode_literals
import json
from flask import Flask, request
from flask_sqlalchemy import SQLAlchemy
from flask_script import Manager, Server
from flask_migrate import Migrate, MigrateCommand
from musicbot import MusicBot
from command_handler import CommandHandler
from raven.contrib.flask import Sentry
from socomubo import SoCoMuBo

app = Flask(__name__)
app.config.from_object('config_default')
app.config.from_object('config_local')

db = SQLAlchemy(app)
migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)

server = Server(host=app.config['HOST'], port=app.config['PORT'])
manager.add_command('runserver', server)

sentry = Sentry(app)
sonos = SoCoMuBo(app.config['DEVICE_IP'])
musicbot = MusicBot(app.config, sonos)
ch = CommandHandler(app.config, musicbot)

@manager.command
def seed_db():
    offset = 0
    while True:
        playlist = musicbot.get_spotify_playlist(app.config['PLAYLIST_USER_ID'], app.config['PLAYLIST_ID'], offset)
        if len(playlist) > 0:
            musicbot.mysql.seed_playlist_table(playlist, 3)
            offset += 100
        else:
            break


# Main route for slack integrations
@app.route('/', methods=['POST'])
def post():
    command = request.form['text'].strip()
    user = request.form['user_name']
    response = ch.command_handler(command, user)
    return post_message(response)


# Health check
@app.route('/ping')
def get():
    return 'pong'

# Route for testing commands, pass the user in the query string
# Example: `localhost:2000/test/current?user=bob` will run the `current` command as user `bob`
@app.route("/test/<command>")
def current(command):
    user = request.args.get('user')
    return ch.command_handler(command, user)

@app.route("/playback-state")
def playback_state():
    return sonos.get_current_transport_info()['current_transport_state']

# Helper Functions
def payload(text):
    return {
        "channel": "#musicbot-channel",
        "username": "MusicBot",
        "text": text,
        "icon_emoji": ":musical_note:",
        'link_names': 1
    }

def post_message(message):
    if message:
        return json.dumps(payload(message))
    return message

def main():
    app.run(host=app.config['HOST'], port=2000, debug=app.config['DEBUG'], use_reloader=False)

# bootstraps app
if __name__ == "__main__":
    manager.run()
