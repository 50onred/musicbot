from __future__ import unicode_literals
from datetime import datetime
from mysql_db import MySqlDb, get_base_track_uri
from urllib import quote_plus
from random import random
from redis_db import RedisDB
from requests.exceptions import RequestException
from base64 import b64encode
import json
import requests

from urlparse import urljoin


class MusicBot(object):
    def __init__(self, config, sonos, session=None):
        self.config = config
        self.sonos = sonos
        self.redis = RedisDB()
        self.mysql = MySqlDb(self.config)
        self.add_requests = []
        self.vetoes = []
        self.vetoed_track_uri = ''
        self.upvoted_track_uri = ''
        self.upvotes = []
        self.record_event = self.mysql.record_event
        self.session = session or requests.Session()
        self.access_token = self.get_spotify_token()
        self.update_session_headers()

    def request(self, method, endpoint, **kwargs):
        response = self.session.request(method, urljoin('https://api.spotify.com/v1/', endpoint), **kwargs)
        if response.status_code == 200:
            return response
        if response.status_code == 401:
            self.update_spotify_token()
            response = self.session.request(method, urljoin('https://api.spotify.com/v1/', endpoint), **kwargs)
            if response.status_code == 200:
                return response
            if response.status_code == 401:
                raise Exception('Spotify request unauthorized after refreshing token. Strange!')

        response.raise_for_status()

    def update_session_headers(self):
        self.session.headers.update({
            'Authorization': 'Bearer {}'.format(self.access_token)
        })

    def get_spotify_token(self):
        client_id = self.config['SPOTIFY_CLIENT_ID']
        client_secret = self.config['SPOTIFY_CLIENT_SECRET']
        header = {'Authorization': 'Basic ' + b64encode((client_id + ':' + client_secret))}
        body = {'grant_type': 'client_credentials'}
        response = requests.request('POST', 'https://accounts.spotify.com/api/token', data=body, headers=header)
        if response.status_code == 200:
            return response.json()['access_token']
        response.raise_for_status()

    def update_spotify_token(self):
        self.access_token = self.get_spotify_token()
        self.update_session_headers()

    def get_playback_state(self):
        return self.sonos.get_current_transport_info()['current_transport_state']

    def play(self):
        self.sonos.play()
        return True

    def pause(self):
        self.sonos.pause()
        return True

    def vol_up(self):
        current_volume = self.sonos.volume
        if current_volume < 100:
            current_volume = min(current_volume + 5, 100)
            self.sonos.volume = current_volume
            return current_volume
        return False

    def vol_down(self):
        current_volume = self.sonos.volume
        if current_volume > 0:
            current_volume = max(current_volume - 5, 0)
            self.sonos.volume = current_volume
            return current_volume
        return False

    def next_track(self):
        self.sonos.next()
        return True

    def prev_track(self):
        self.sonos.previous()
        return True

    def get_current_track(self):
        return self.get_track_metadata(decode_dict_values(self.sonos.get_current_track_info()))

    def get_queue(self, length=10, start=None):
        return self.sonos.get_queue(start=start if isinstance(start, int) else self.get_queue_position(),
                                    max_items=length)

    def get_queue_position(self):
        return self.sonos.get_current_track_info()['playlist_position']

    def get_queue_length(self):
            return self.sonos.queue_size

    def clear_queue(self):
        return self.sonos.clear_queue()

    def remove_from_queue(self, index=0):
        current = int(self.get_queue_position()) - 1
        self.sonos.remove_from_queue(current + index)

    def get_next_on_queue(self, length=10):
        return [self.get_track_metadata(decode_dict_values(track.to_dict)) for track in self.get_queue(length)]

    def get_track_metadata(self, track):
        sql_metadata = self.mysql.get_track_by_uri(track['uri']) or {}
        mubo_track = track['title'] == '' or track['title'].startswith('x-sonos-spotify')
        return {
            'uri': get_base_track_uri(track['uri']),
            'title': sql_metadata.get('title', 'Unknown') if mubo_track else track['title'],
            'artist': track.get('artist') or track.get('creator') or sql_metadata.get('artist', 'Unknown'),
            'popularity': sql_metadata.get('popularity', 0),
            'last_added_by': sql_metadata.get('last_added_by', 'MusicBot' if mubo_track else 'Sonos'),
            'playlist_position': track.get('playlist_position') or int(track['item_id'].split('/')[1]),
            'last_added_at': track.get('last_added_at', 'Unknown')
        }

    def veto(self, user):
        current_track_data = self.get_track_metadata(self.sonos.get_current_track_info())
        if not current_track_data['uri'] == self.vetoed_track_uri:
            self.vetoes = []
            self.vetoed_track_uri = current_track_data['uri']

        if user in self.vetoes:
            return False, current_track_data

        self.vetoes.append(user)
        self.record_event('veto', uri=current_track_data['uri'], user=user)
        num = len(self.vetoes)

        if num == self.config['VETOES_REQUIRED']:
            self.vetoes = []
            current_track_data['popularity'] = self.decrease_popularity(current_track_data['uri'])
            self.sonos.next()

            # if we veto, count it for the original user
            requester = self.redis.get_last_requester(current_track_data['uri'])
            if requester:
                self.redis.increment_user_vetos(requester)
                self.redis.increment_user_ban(requester)

        return num, current_track_data

    def upvote(self, user):
        current_track_data = self.get_track_metadata(self.sonos.get_current_track_info())
        if not current_track_data['uri'] == self.upvoted_track_uri:
            self.upvotes = []
            self.upvoted_track_uri = current_track_data['uri']

        if user in self.upvotes:
            return False, current_track_data

        self.upvotes.append(user)
        self.record_event('upvote', uri=current_track_data['uri'], user=user)
        num = len(self.upvotes)

        if num == self.config['UPVOTES_REQUIRED']:
            current_track_data['popularity'] = self.increase_popularity(current_track_data['uri'])

            # if we upvote, clear the original user of all bans
            requester = self.redis.get_last_requester(current_track_data['uri'])
            if requester:
                self.redis.clear_user_vetos(requester)

        return num, current_track_data

    def increase_popularity(self, uri):
        new_popularity = self.mysql.increase_popularity(uri)
        self.record_event('popularity_increased', uri=uri, popularity=new_popularity)
        return new_popularity

    def decrease_popularity(self, uri):
        new_popularity = self.mysql.decrease_popularity(uri)
        self.record_event('popularity_decreased', uri=uri, popularity=new_popularity)
        return new_popularity

    def search_for_track(self, query, user, limit=5):
        params = {
            'q': query.replace(' ', '+'),
            'type': 'track',
            'limit': limit,
            'market': self.config['COUNTRY_CODE']
        }
        response = self.request('GET', 'search', params=params).json()
        tracks = response['tracks']['items']

        if not tracks:
            return False

        add_request = {'user': user, 'results': []}
        for x in tracks:
            title = x['name']
            artist = x['artists'][0]['name']
            uri = x['uri']
            explicit = x['explicit']
            track = {'title': title, 'artist': artist, 'uri': uri, 'explicit': explicit}
            add_request['results'].append(track)

        self.add_requests = filter(lambda req: req.get('user') != user, self.add_requests)

        self.add_requests.append(add_request)
        return add_request['results']

    def add_chosen_track(self, choice, user, hour):
        current_request = {}
        for i in range(0, len(self.add_requests)):
            current_request = self.add_requests[i]
        try:
            if current_request['user'] == user:
                choice = int(choice) - 1

                chosen_track = current_request['results'][choice]

                if chosen_track['explicit'] and self.is_work_hours(hour):
                    return 'explicit_track'

                self.add_uri_to_sonos(chosen_track['uri'])

                self.add_requests.pop(i)

                self.record_event('add', uri=chosen_track['uri'], user=user)

                # record song add for this user
                self.redis.set_last_requester(chosen_track['uri'], user)
                self.mysql.add_new_track(chosen_track, user)

                return chosen_track
            return False
        except KeyError:
            return False

    def add_uri_to_sonos(self, uri):
        qp = self.get_queue_position()
        for _ in range(self.sonos.queue_size - 1):
            track = self.get_next_on_queue(1).pop()
            if not track or track['last_added_by'] in ['MusicBot', 'Sonos', 'Unknown']:
                break

            qp = track['playlist_position']

        return self.sonos.add_uri_to_queue('x-sonos-spotify:{}?sid=12&flags=32'.format(quote_plus(uri)), qp)



    def get_spotify_playlist(self, user_id, playlist_id, offset):
        response = self.request('GET',
                                'https://api.spotify.com/v1/users/' + user_id + '/playlists/' + playlist_id +
                                '/tracks?offset=' + str(offset) + '&market=' + self.config['COUNTRY_CODE']
                                )
        return response.json()['items']

    def get_spotify_track(self, uri):
        uri = uri.replace('spotify:track:', '')
        response = self.request('GET',
                                'https://api.spotify.com/v1/tracks/' + uri)
        return response.json()

    def add_random_track(self):
        tracks = self.mysql.get_early_tracks(self.config['EARLY_LIMIT'], self.is_work_hours(datetime.now().time().hour))
        top_index = None
        top_score = 0
        for i in range(len(tracks)):
            score = (tracks[i]['popularity'] / 2) * random()
            if score > top_score:
                top_score = score
                top_index = i
        try:
            if not self.is_valid_uri(tracks[top_index]['uri']):
                self.mysql.delete_track_by_uri(tracks[top_index]['uri'])
                return self.add_random_track()
        except RuntimeError:
            return
        self.add_uri_to_sonos(tracks[top_index]['uri'])
        self.record_event('add', uri=tracks[top_index]['uri'], user='MusicBot')
        self.mysql.update_time_and_user(tracks[top_index]['uri'], 'MusicBot')
        return tracks[top_index]['uri']

    def is_valid_uri(self, uri):
        uri = uri.replace('spotify:track:', '')
        try:
            response = self.request('GET','https://api.spotify.com/v1/tracks/' + uri)
            data = json.loads(response.text)
        except (RequestException, ValueError, TypeError):
            raise RuntimeError('Could not connect to Spotify API or received invalid data.')

        return response.ok and self.config['COUNTRY_CODE'] in data['available_markets']

    def man(self, is_admin):
        to_return = self.config['HELP']

        if is_admin:
            to_return += self.config['ADMIN_HELP']

        return to_return

    def is_user_banned(self, user):
        banned, ttl = self.redis.get_user_ban(user)
        if banned:
            return True, humanize_time(ttl, 'seconds')
        return False, ''

    def is_work_hours(self, hour):
        return self.config['WORK_HOURS_START'] <= hour < self.config['WORK_HOURS_END']


def humanize_time(amount, units):
    '''
    Divide `amount` in time periods.
    Useful for making time intervals more human readable.

    >>> humanize_time(173, 'hours')
    [(1, 'week'), (5, 'hours')]
    >>> humanize_time(17313, 'seconds')
    [(4, 'hours'), (48, 'minutes'), (33, 'seconds')]
    >>> humanize_time(90, 'weeks')
    [(1, 'year'), (10, 'months'), (2, 'weeks')]
    >>> humanize_time(42, 'months')
    [(3, 'years'), (6, 'months')]
    >>> humanize_time(500, 'days')
    [(1, 'year'), (5, 'months'), (3, 'weeks'), (3, 'days')]
    '''
    INTERVALS = [1, 60, 3600, 86400, 604800, 2419200, 29030400]
    NAMES = [('second', 'seconds'),
             ('minute', 'minutes'),
             ('hour', 'hours'),
             ('day', 'days'),
             ('week', 'weeks'),
             ('month', 'months'),
             ('year', 'years')]
    result = []

    unit = map(lambda a: a[1], NAMES).index(units)
    # Convert to seconds
    amount = amount * INTERVALS[unit]

    for i in range(len(NAMES)-1, -1, -1):
        a = amount / INTERVALS[i]
        if a > 0:
            result.append( '{} {}'.format(a, NAMES[i][1 % a]) )
            amount -= a * INTERVALS[i]

    return ', '.join(result)


def decode_dict_values(dict):
    return {k: v.decode('utf-8') for k,v in dict.iteritems()}