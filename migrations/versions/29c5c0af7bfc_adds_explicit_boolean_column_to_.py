"""Adds explicit boolean column to playlist table

Revision ID: 29c5c0af7bfc
Revises: 28561b0103d5
Create Date: 2015-08-12 14:24:44.876415

"""

# revision identifiers, used by Alembic.
revision = '29c5c0af7bfc'
down_revision = '28561b0103d5'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('playlist', sa.Column('explicit', sa.Boolean, server_default='0', nullable=False))


def downgrade():
    op.drop_column('playlist', 'explicit')
