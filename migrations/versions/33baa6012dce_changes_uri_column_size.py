"""Changes uri column size

Revision ID: 33baa6012dce
Revises: 2fee0eb28305
Create Date: 2015-07-27 15:05:06.392310

"""

# revision identifiers, used by Alembic.
revision = '33baa6012dce'
down_revision = '2fee0eb28305'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.alter_column('playlist', 'uri',
                    type_=sa.Unicode(36),
                    existing_nullable=False)

def downgrade():
    op.alter_column('playlist', 'uri',
                    type_=sa.Unicode(22),
                    existing_nullable=False)

