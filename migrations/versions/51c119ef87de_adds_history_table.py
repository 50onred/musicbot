"""Adds history table

Revision ID: 51c119ef87de
Revises: 29c5c0af7bfc
Create Date: 2015-08-26 16:35:48.771908

"""

# revision identifiers, used by Alembic.
revision = '51c119ef87de'
down_revision = '29c5c0af7bfc'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'history',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('action', sa.Unicode(50), nullable=False),
        sa.Column('user', sa.Unicode(50)),
        sa.Column('uri', sa.Unicode(36)),
        sa.Column('timestamp', sa.DateTime(), nullable=False)
    )


def downgrade():
    op.drop_table('history')
