"""Adds popularity column to history table

Revision ID: 1c44b6c46975
Revises: 51c119ef87de
Create Date: 2015-08-28 11:03:04.900509

"""

# revision identifiers, used by Alembic.
revision = '1c44b6c46975'
down_revision = '51c119ef87de'

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.add_column('history', sa.Column('popularity', sa.Integer))


def downgrade():
    op.drop_column('history', 'popularity')
