"""create playlist table

Revision ID: 2fee0eb28305
Revises: 
Create Date: 2015-07-25 22:30:23.426719

"""

# revision identifiers, used by Alembic.
revision = '2fee0eb28305'
down_revision = None
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.create_table(
        'playlist',
        sa.Column('id', sa.Integer, primary_key=True),
        sa.Column('uri', sa.String(22), nullable=False, unique=True),
        sa.Column('title', sa.Unicode(100), nullable=False),
        sa.Column('artist', sa.Unicode(100), nullable=False),
        sa.Column('last_added_at', sa.DateTime()),
        sa.Column('last_added_by', sa.Unicode(50)),
        sa.Column('popularity', sa.Integer(), nullable=False, server_default='0'),
    )


def downgrade():
    op.drop_table('playlist')
