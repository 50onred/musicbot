"""Changes default popularity for new entries to 3

Revision ID: 28561b0103d5
Revises: 33baa6012dce
Create Date: 2015-07-29 10:31:43.645703

"""

# revision identifiers, used by Alembic.
revision = '28561b0103d5'
down_revision = '33baa6012dce'
branch_labels = None
depends_on = None

from alembic import op
import sqlalchemy as sa


def upgrade():
    op.alter_column('playlist', 'popularity',
                    server_default='3')


def downgrade():
    pass
