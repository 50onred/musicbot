from datetime import datetime
from sqlalchemy import *
from sqlalchemy import exc
from urllib import unquote


class MySqlDb(object):
    def __init__(self, config):
        self.engine = create_engine(config['SQLALCHEMY_DATABASE_URI'], encoding='utf8', pool_recycle=3600)
        self.meta = MetaData()
        try:
            self.playlist = Table('playlist', self.meta, autoload=True, autoload_with=self.engine)
        except exc.NoSuchTableError:
            self.playlist = None
        try:
            self.history = Table('history', self.meta, autoload=True, autoload_with=self.engine)
        except exc.NoSuchTableError:
            self.history = None
        self.conn = self.engine.connect()
        self.config = config

    def seed_playlist_table(self, playlist_data, popularity_default):
        for item in playlist_data:
            track = item['track']
            uri = get_base_track_uri(track['uri'])
            title = (track['name'][:100]).encode('utf8')
            artists = map(lambda a: a['name'], track['artists'])
            artist_string = " & ".join(artists)
            artist_string = (artist_string[:100]).encode('utf8')
            explicit = False
            if track['explicit']:
                explicit = True
            ins = self.playlist.insert().values(uri=uri,
                                                title=title,
                                                artist=artist_string,
                                                popularity=popularity_default,
                                                explicit=explicit)
            try:
                self.conn.execute(ins)
                print 'inserting track', uri, title, artist_string
            except exc.IntegrityError:
                print 'duplicate track, skipping!!', uri, title, artist_string
                pass

    def get_early_tracks(self, limit, is_work_hours):
        if is_work_hours:
            s = select([self.playlist.c.uri, self.playlist.c.popularity, self.playlist.c.explicit])\
                .where(self.playlist.c.explicit == False)\
                .order_by('last_added_at')\
                .limit(limit)
        else:
            s = select([self.playlist.c.uri, self.playlist.c.popularity, self.playlist.c.explicit])\
                .order_by('last_added_at').limit(limit)
        results = self.conn.execute(s)
        tracks = []
        for row in results:
            track = {
                'uri': row['uri'],
                'popularity': row['popularity'],
                'explicit': row['explicit']
            }
            tracks.append(track)
        results.close()
        return tracks

    def add_new_track(self, track_data, user):
        uri = get_base_track_uri(track_data['uri'])
        ins = self.playlist.insert().values(uri=uri,
                                            title=track_data['title'],
                                            artist=track_data['artist'],
                                            last_added_by=user,
                                            last_added_at=datetime.utcnow(),
                                            explicit=track_data['explicit'])
        try:
            self.conn.execute(ins)
            return 'inserted'
        except exc.IntegrityError:
            upd = self.playlist.update()\
                .where(self.playlist.c.uri == uri)\
                .values(last_added_by=user,
                        last_added_at=datetime.utcnow())
            self.conn.execute(upd)
            return 'updated'

    def get_track_by_uri(self, uri):
        uri = get_base_track_uri(uri)
        s = select([self.playlist]).where(self.playlist.c.uri == uri)
        row = self.conn.execute(s).first()
        if not row:
            return None
        track = {
            'uri': row['uri'],
            'title': row['title'],
            'artist': row['artist'],
            'last_added_at': row['last_added_at'],
            'last_added_by': row['last_added_by'],
            'popularity': row['popularity']
        }
        return track

    def delete_track_by_uri(self, uri):
        uri = get_base_track_uri(uri)
        self.conn.execute(self.playlist.delete()
                          .where(self.playlist.c.uri == uri))

    def decrease_popularity(self, uri):
        uri = get_base_track_uri(uri)
        s = select([self.playlist]).where(self.playlist.c.uri == uri)
        row = self.conn.execute(s).first()
        if row['popularity'] == 0:
            return 0
        popularity = row['popularity'] - 1
        upd = self.playlist.update().where(self.playlist.c.uri == uri).values(popularity=popularity)
        self.conn.execute(upd)
        return popularity

    def increase_popularity(self, uri):
        uri = get_base_track_uri(uri)
        s = select([self.playlist]).where(self.playlist.c.uri == uri)
        row = self.conn.execute(s).first()
        if row['popularity'] == 5:
            return False
        popularity = row['popularity'] + 1
        upd = self.playlist.update().where(self.playlist.c.uri == uri).values(popularity=popularity)
        self.conn.execute(upd)
        return popularity

    def update_time_and_user(self, uri, user):
        uri = get_base_track_uri(uri)
        upd = self.playlist.update().where(self.playlist.c.uri == uri).values(last_added_by=user,
                                                                              last_added_at=datetime.utcnow())
        self.conn.execute(upd)

    def record_event(self, action, user=None, uri=None, popularity=None):
        if uri:
            uri = get_base_track_uri(uri)
        ins = self.history.insert().values(action=action,
                                           user=user,
                                           uri=uri,
                                           timestamp=datetime.utcnow(),
                                           popularity=popularity)
        return self.conn.execute(ins)

    def get_user_add_count(self, user):
        current_time = datetime.utcnow()
        timeframe = current_time - datetime.timedelta(minutes=self.config['ADD_RESTRICTION_TIMEFRAME'])
        sel = select([self.history.c.user, func.count(self.history.c.user)])\
            .where(self.history.c.timestamp >= timeframe)\
            .group_by(self.history.c.user)
        result = self.conn.execute(sel)
        return result.row()[user]


def get_base_track_uri(uri=''):
    return unquote(uri).replace('x-sonos-spotify:', '').split('?')[0]